package com.example.qwerty.scheduledmessenger;

import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mContact, mMessage;
    Button mfinish;
    String name = null, phoneNumber = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().hide();

        mContact = (EditText)findViewById(R.id.etContact);
        mMessage = (EditText)findViewById(R.id.etMessage);
        mfinish = (Button)findViewById(R.id.btnFinish);

        mContact.setOnClickListener(this);
        mfinish.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.etContact:
                Intent setContact = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                setContact.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(setContact, 1);
                break;
            case R.id.btnFinish:
                Intent intent = new Intent();
                intent.putExtra("name", name);
                intent.putExtra("number", phoneNumber);
                intent.putExtra("message", mMessage.getText().toString());
                setResult(1, intent);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            String contactId = null;

            Cursor cursor = getContentResolver().query(data.getData(), null, null, null, null);
            Cursor cursorID = getContentResolver().query(data.getData(),
                    new String[]{ContactsContract.Contacts._ID},
                    null, null, null);

            if (cursorID.moveToFirst()) {
                contactId = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
            }

            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }

            cursor.close();
            Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                            ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                    new String[]{contactId},
                    null);

            if (cursorPhone.moveToFirst()) {
                phoneNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }

            cursorPhone.close();

            mContact.setText(name + " - " + phoneNumber);

        }
    }
}
