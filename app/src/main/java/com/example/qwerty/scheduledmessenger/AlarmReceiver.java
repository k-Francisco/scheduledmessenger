package com.example.qwerty.scheduledmessenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by qwerty on 8/22/2016.
 */
public class AlarmReceiver extends BroadcastReceiver{


    @Override
    public void onReceive(Context context, Intent intent) {

        String get_your_string = intent.getExtras().getString("extra");
        String number = intent.getExtras().getString("number");
        String message = intent.getExtras().getString("message");

        Intent service_intent = new Intent(context, RingtonePlayingService.class);

        service_intent.putExtra("extra", get_your_string);
        service_intent.putExtra("number", number);
        service_intent.putExtra("message", message);

        context.startService(service_intent);

    }
}
