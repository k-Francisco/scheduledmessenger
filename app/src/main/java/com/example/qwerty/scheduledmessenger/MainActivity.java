package com.example.qwerty.scheduledmessenger;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ArrayList<String> list = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    TextView noSM;
    SharedPreferences sharedPreferences;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    String name=null, number=null, message=null, hour = null, minute = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        ListView mList = (ListView) findViewById(R.id.listView);
        mList.setAdapter(adapter);
        mList.setOnItemClickListener(this);
        noSM = (TextView)findViewById(R.id.tvNoSm);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.plus);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AlarmActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        if(list.size()==0){
            noSM.setText("Please Click the + button to add scheduled messages");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
             number = data.getStringExtra("number");
            String number2 = data.getStringExtra("number");
            String message2 = data.getStringExtra("message");
             name = data.getStringExtra("name");
             message = data.getStringExtra("message");
            hour = data.getStringExtra("hour");
            minute = data.getStringExtra("minute");
             addItem(number, name, message, hour, minute);
            sharedPreferences = getSharedPreferences(MY_PREFS_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("number", number2);
            editor.putString("message", message2);
            editor.commit();
        }


    }


        public void addItem(String number, String name, String message, String hour, String minute){
        list.add(name + "-" + number + "\n" + message + "\n" + hour + ":" + minute);
        adapter.notifyDataSetChanged();
        noSM.setText("");
    }
}
