package com.example.qwerty.scheduledmessenger;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {

    Button mSet, mDone, mSendSMs;
    String name=null, phoneNumber=null, message = null, hours=null, minutes = null;
    AlarmManager alarm_manager;
    TimePicker alarm_timepicker;
    Context context;
    PendingIntent pending_intent;
    public static final String MY_PREFS_NAME = "MyPrefsFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        getSupportActionBar().hide();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, 0);
        phoneNumber = prefs.getString("number", null);
        message = prefs.getString("message", null);
        this.context = this;

        alarm_manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm_timepicker = (TimePicker) findViewById(R.id.timePicker);

        final Intent my_Intent = new Intent(this.context, AlarmReceiver.class);
        final Calendar calendar = Calendar.getInstance();


        mSet = (Button)findViewById(R.id.btnSet);
        mDone = (Button)findViewById(R.id.btnDone);
        mSendSMs = (Button)findViewById(R.id.btnCancel);

        mSendSMs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                SmsManager manager=SmsManager.getDefault();
//                manager.sendTextMessage(phoneNumber, null, message, null, null);

                Uri uri = Uri.parse("smsto:" + phoneNumber);
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.putExtra("sms_body", message);
                startActivity(intent);

                alarm_manager.cancel(pending_intent);
                my_Intent.putExtra("extra", "alarm off");
                sendBroadcast(my_Intent);
            }
        });

        mSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AlarmActivity.this, DetailsActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                calendar.set(Calendar.HOUR_OF_DAY, alarm_timepicker.getCurrentHour());
                calendar.set(Calendar.MINUTE, alarm_timepicker.getCurrentMinute());

                int hour = alarm_timepicker.getCurrentHour();
                int minute = alarm_timepicker.getCurrentMinute();

                hours = String.valueOf(hour);
                minutes = String.valueOf(minute);

                if (hour > 12) {
                    hours = String.valueOf(hour - 12);
                }

                if (minute < 10) {
                    minutes = "0" + String.valueOf(minute);
                }

                Intent done = new Intent();
                done.putExtra("name", name);
                done.putExtra("number", phoneNumber);
                done.putExtra("message", message);
                done.putExtra("hour", hours);
                done.putExtra("minute", minutes);
                setResult(1, done);
                finish();


                my_Intent.putExtra("extra", "alarm on");
                my_Intent.putExtra("number", phoneNumber);
                my_Intent.putExtra("message", message);



                pending_intent = PendingIntent.getBroadcast(AlarmActivity.this, 0,
                        my_Intent, PendingIntent.FLAG_UPDATE_CURRENT);

                alarm_manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        pending_intent);



            }
        });

    }


    @Override
    protected void onResume() {

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, 0);
        phoneNumber = prefs.getString("number", null);
        message = prefs.getString("message", null);
        super.onResume();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            name = data.getStringExtra("name");
            phoneNumber = data.getStringExtra("number");
            message = data.getStringExtra("message");
        }
    }
}
