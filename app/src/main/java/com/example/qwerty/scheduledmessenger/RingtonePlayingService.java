package com.example.qwerty.scheduledmessenger;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by qwerty on 8/22/2016.
 */
public class RingtonePlayingService extends Service{

    MediaPlayer media_song;
    int startId;
    boolean isRunning;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        String state = intent.getExtras().getString("extra");
        String number = intent.getExtras().getString("number");
        String message = intent.getExtras().getString("message");





        NotificationManager notify_manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        Intent intent_sendSms_activity = new Intent(this, AlarmActivity.class);
        PendingIntent pending_intent_main_activity = PendingIntent.getActivity(this, 0,
                intent_sendSms_activity, 0);

        Notification notification_popup = new Notification.Builder(this)
                .setContentTitle(number)
                .setContentText("Click me!")
                .setSmallIcon(R.drawable.plus)
                .setContentIntent(pending_intent_main_activity)
                .setAutoCancel(true)
                .build();



        assert state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                break;
            default:
                startId = 0;
                break;
        }


        if(!this.isRunning && startId == 1){
            Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);
            notify_manager.notify(0, notification_popup);
            media_song = MediaPlayer.create(this, R.raw.tadhanda);
            media_song.start();
            this.isRunning = true;
            startId = 0;
        }
        else if(this.isRunning && startId == 0){

            media_song.stop();
            media_song.reset();

            this.isRunning = false;
            startId = 0;
        }
        else if(!this.isRunning && startId == 0){

        }
        else if(this.isRunning && startId == 1){


        }




        return START_NOT_STICKY;
    }

}
